package today.dotag.dotag.common;

public class Constant {
    public static class Notification {
        public static final int FOREGROUND_SERVICE = 123;
    }

    public static class PrefKey {
        public static final String token = "token";
    }

    public static final String EXTRA_OBJECT = "extra_object";
}
