package today.dotag.dotag.common;

import android.app.NotificationManager;
import android.content.Context;

import java.util.concurrent.atomic.AtomicInteger;

import today.dotag.dotag.R;

public class Notification {

    private static AtomicInteger notificationID;

    static {
        notificationID = new AtomicInteger();
    }


    public static void info(Context context, String msg) {
        NotificationManager manager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        android.app.Notification notificaiton = NotificationFactory.simpleNotification(context,
                context.getString(R.string.app_name),
                msg);

        if (manager != null) {
            manager.notify(incrementAndGet(), notificaiton);
        }
    }

    private static int incrementAndGet() {
        return notificationID.incrementAndGet();
    }
}
