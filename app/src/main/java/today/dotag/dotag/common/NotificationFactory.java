package today.dotag.dotag.common;

import android.app.Notification;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import today.dotag.dotag.R;

public class NotificationFactory {

    public static Notification simpleNotification(Context context, String title, String message) {
        return new NotificationCompat.Builder(context)
                .setContentTitle(title)
                .setTicker(title)
                .setContentText(message)
                .setSmallIcon(R.mipmap.ic_launcher).build();


    }
}
