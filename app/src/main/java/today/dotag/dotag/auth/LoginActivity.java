package today.dotag.dotag.auth;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.orhanobut.logger.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;
import today.dotag.dotag.HomeActivity;
import today.dotag.dotag.R;
import today.dotag.dotag.alarm.AlarmReceiver;
import today.dotag.dotag.common.Constant;
import today.dotag.dotag.common.SharedPreferenceUtils;
import today.dotag.dotag.network.ApiInterface;
import today.dotag.dotag.network.ServiceGenerator;

public class LoginActivity extends AppCompatActivity {

    private static final String EMAIL = "email";
    private static final String FRIENDS = "user_friends";
    private CallbackManager callbackManager;

    @BindView(R.id.login_button)
    public LoginButton loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        //setting alarm for test but it must be done after login
        AlarmReceiver alarmReceiver = new AlarmReceiver();
        alarmReceiver.setAlarm(this);

        String token = SharedPreferenceUtils.getFromPrefs(getApplicationContext(),
                Constant.PrefKey.token,
                "");

        if (TextUtils.isEmpty(token)) {
            setupFacebookAuth();
        } else {
            HomeActivity.start(LoginActivity.this);
        }
    }

    private void setupFacebookAuth() {
        callbackManager = CallbackManager.Factory.create();
        loginButton.setReadPermissions(Arrays.asList(EMAIL, FRIENDS));

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Timber.i("Facebook returned %s", loginResult.getAccessToken().getToken());
                String fbId = loginResult.getAccessToken().getUserId();

                ServiceGenerator
                        .createService(ApiInterface.class)
                        .getAuthToken(loginResult.getAccessToken().getToken())
                        .enqueue(new Callback<AuthResponse>() {
                            @Override
                            public void onResponse(@NonNull Call<AuthResponse> call, @NonNull Response<AuthResponse> response) {
                                if (response.body() == null) {
                                    LoginManager.getInstance().logOut();
                                    info(getString(R.string.uknown_error_occured));
                                }else {

                                    Timber.i("DoTag returned %s", response.body().getToken());
                                    SharedPreferenceUtils.saveToPrefs(getApplicationContext(),
                                            Constant.PrefKey.token,
                                            response.body().getToken());

                                    HomeActivity.start(LoginActivity.this);

                                }
                            }

                            @Override
                            public void onFailure(Call<AuthResponse> call, Throwable t) {
                                info(t.getMessage());
                                LoginManager.getInstance().logOut();
                            }
                        });

            }

            @Override
            public void onCancel() {
                Timber.i("onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                info(error.toString());
                Timber.i("error %s", error.toString());
            }
        });
    }


    private void info(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private void getFacebookFriendsWithDotag(AccessToken accessToken, String userId) {
        //https://developers.facebook.com/docs/graph-api/reference/v2.2/user/friends

        String url = String.format("/%s/friends", userId);
        Timber.i("Hitting %s", url);
        GraphRequest request = GraphRequest.newGraphPathRequest(
                accessToken,
                url,
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse response) {
                        // Insert your code here
                        String json = response.getJSONObject().toString();
                        Logger.d(json);

                        finish();
                        HomeActivity.start(LoginActivity.this);


                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", FRIENDS);
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void getSelfFacebookInfo(AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    //You can fetch user info like this…
                    object.getJSONObject("picture");
                    object.getString("name");
                    object.getString("email");
                    String fbId = object.getString("id");
                    Timber.i(fbId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,picture.width(200)");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);

        super.onActivityResult(requestCode, resultCode, data);
    }
}
