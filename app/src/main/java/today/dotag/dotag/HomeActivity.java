package today.dotag.dotag;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import today.dotag.dotag.components.DoTagButton;
import today.dotag.dotag.map.MapActivity;
import today.dotag.dotag.rating.RatingActivity;

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.btn_dotag_activity_home)
    DoTagButton doTagButton;

    public static void start(Activity activity) {
        WeakReference<Activity> context = new WeakReference<Activity>(activity);

        Intent intent = new Intent(context.get(), HomeActivity.class);
        context.get().startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
    }


    @OnClick({R.id.btn_dotag_activity_home, R.id.btn_map_activity_home})
    public void onListItemTap(View view) {
        switch (view.getId()) {
            case R.id.btn_dotag_activity_home:
                RatingActivity.start(HomeActivity.this);
                break;
            case R.id.btn_map_activity_home:
                MapActivity.start(HomeActivity.this);
                break;
        }

    }


}


