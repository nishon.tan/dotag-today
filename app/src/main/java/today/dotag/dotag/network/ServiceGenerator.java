package today.dotag.dotag.network;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.readystatesoftware.chuck.ChuckInterceptor;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import today.dotag.dotag.application.Dotag;
import today.dotag.dotag.common.Constant;
import today.dotag.dotag.common.SharedPreferenceUtils;

public class ServiceGenerator {
    private final static String BASE_API_URL = "http://10.0.2.2:8000/";
    private static Retrofit retrofit = null;
    private static Gson gson = new GsonBuilder().create();


    private static Interceptor createAuthInterceptor(final String token) {

        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder()
                        .addHeader("Authorization",
                                "Token " + token)
                        .build();
                return chain.proceed(request);
            }
        };
    }

    private static OkHttpClient createOkHttpClient() {
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
        String token = SharedPreferenceUtils.getFromPrefs(Dotag.getInstance(), Constant.PrefKey.token, "");

        if (!TextUtils.isEmpty(token)) {
            okHttpClientBuilder.addInterceptor(createAuthInterceptor(token));
        }

        return okHttpClientBuilder
                .addInterceptor(new ChuckInterceptor(Dotag.getInstance()))
                .build();
    }

    public static <T> T createService(Class<T> serviceClass) {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .client(createOkHttpClient())
                    .baseUrl(BASE_API_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit.create(serviceClass);
    }

}
