package today.dotag.dotag.network;

import android.database.Observable;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import today.dotag.dotag.auth.AuthResponse;
import today.dotag.dotag.rating.RatingResponse;

public interface ApiInterface {
    @GET("/api/fbtest/facebook/")
    Call<AuthResponse> getAuthToken(@Query("access_token") String token);

    @POST("/apiv2/reportrating/")
    Call<RatingResponse> postRating(@Body RequestBody body);


}
