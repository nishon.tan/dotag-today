package today.dotag.dotag.rating;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobRequest;
import com.evernote.android.job.util.support.PersistableBundleCompat;
import com.google.gson.Gson;

import java.net.HttpURLConnection;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.HTTP;
import today.dotag.dotag.common.Constant;
import today.dotag.dotag.network.ApiInterface;
import today.dotag.dotag.network.ServiceGenerator;

import static java.net.HttpURLConnection.HTTP_CREATED;

public class RatingSyncJob extends Job {

    public static final String TAG = "rating_sync_job_tag";
    private Result jobStatus;


    @Override
    @NonNull
    protected Result onRunJob(Params params) {
        String rating = params.getExtras().getString(Constant.EXTRA_OBJECT, "");
        if (TextUtils.isEmpty(rating)) {
            jobStatus = Result.FAILURE;
            return jobStatus;
        }

        uploadRating(rating);
        return jobStatus;
    }

    private void uploadRating(String rating) {
        RatingResponse ratingResponse = new Gson().fromJson(rating, RatingResponse.class);
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("lat", ratingResponse.getLatitude())
                .addFormDataPart("lon", ratingResponse.getLongitude())
                .addFormDataPart("air_pollution_rating", ratingResponse.getAirPollution())
                .addFormDataPart("sound_pollution_rating", ratingResponse.getSoundPollution())
                .addFormDataPart("water_pollution_rating", ratingResponse.getWaterPollution())
                .addFormDataPart("land_pollution_rating", ratingResponse.getLandPollution())
                .build();

        ServiceGenerator
                .createService(ApiInterface.class)
                .postRating(requestBody)
                .enqueue(new Callback<RatingResponse>() {
                    @Override
                    public void onResponse(Call<RatingResponse> call, Response<RatingResponse> response) {
                        switch (response.code()) {
                            case (HTTP_CREATED):
                                jobStatus = Result.SUCCESS;
                                break;
                            default:
                                jobStatus = Result.FAILURE;
                                //todo rescedule
                                break;
                        }
                    }

                    @Override
                    public void onFailure(Call<RatingResponse> call, Throwable t) {
                        jobStatus = Result.FAILURE;
                    }
                });


    }

    public static void scheduleJob() {
        new JobRequest.Builder(RatingSyncJob.TAG)
                .setExecutionWindow(30_000L, 40_000L)
                .build()
                .schedule();
    }

    public static void runNow(RatingResponse ratingResponse) {
        PersistableBundleCompat extras = new PersistableBundleCompat();
        extras.putString(Constant.EXTRA_OBJECT, new Gson().toJson(ratingResponse));

        int jobId = new JobRequest.Builder(RatingSyncJob.TAG)
                .setExtras(extras)
                .startNow()
                .build()
                .schedule();
    }
}
