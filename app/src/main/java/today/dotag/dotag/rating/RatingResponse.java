package today.dotag.dotag.rating;

import com.google.gson.annotations.SerializedName;

public class RatingResponse {

    @SerializedName("land_pollution_rating")
    public String landPollution = "0";

    @SerializedName("air_pollution_rating")
    public String airPollution = "0";

    @SerializedName("sound_pollution_rating")
    public String soundPollution = "0";

    @SerializedName("water_pollution_rating")
    public String waterPollution = "0";

    @SerializedName("lat")
    public String latitude = "0";

    @SerializedName("lon")
    public String longitude = "0";

    @Override
    public String toString() {
        return "RatingResponse{" +
                "landPollution=" + landPollution +
                ", airPollution=" + airPollution +
                ", soundPollution=" + soundPollution +
                ", waterPollution=" + waterPollution +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }

    public void setLandPollution(String landPollution) {
        this.landPollution = landPollution;
    }

    public void setAirPollution(String airPollution) {
        this.airPollution = airPollution;
    }

    public void setSoundPollution(String soundPollution) {
        this.soundPollution = soundPollution;
    }

    public void setWaterPollution(String waterPollution) {
        this.waterPollution = waterPollution;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLandPollution() {
        return landPollution;
    }

    public String getAirPollution() {
        return airPollution;
    }

    public String getSoundPollution() {
        return soundPollution;
    }

    public String getWaterPollution() {
        return waterPollution;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }
}
