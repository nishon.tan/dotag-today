package today.dotag.dotag.rating;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;


import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import today.dotag.dotag.R;
import today.dotag.dotag.components.DoTagSlider;

public class RatingActivity extends AppCompatActivity {

    @BindView(R.id.slider_land_pollution)
    DoTagSlider sliderLandPollution;

    @BindView(R.id.slider_air_pollution)
    DoTagSlider sliderAirPollution;

    @BindView(R.id.slider_sound_pollution)
    DoTagSlider sliderSoundPollution;

    @BindView(R.id.slider_water_pollution)
    DoTagSlider sliderWaterPollution;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private RatingResponse rating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        ButterKnife.bind(this);
        rating = new RatingResponse();
        rating.setLatitude("0");
        rating.setLongitude("0");

        setupToolbar();
        setupSliders();
    }

    private void setupSliders() {
        sliderSoundPollution
                .setOnSliderChangeListener(new DoTagSlider.valueChangeListener() {
                    @Override
                    public void onSliderChangeListener(String progress) {
                        rating.setSoundPollution(progress);
                    }
                });

        sliderAirPollution.setOnSliderChangeListener(new DoTagSlider.valueChangeListener() {
            @Override
            public void onSliderChangeListener(String progress) {
                rating.setAirPollution(progress);
            }
        });

        sliderLandPollution.setOnSliderChangeListener(new DoTagSlider.valueChangeListener() {
            @Override
            public void onSliderChangeListener(String progress) {
                rating.setLandPollution(progress);
            }
        });

        sliderWaterPollution.setOnSliderChangeListener(new DoTagSlider.valueChangeListener() {
            @Override
            public void onSliderChangeListener(String progress) {
                rating.setWaterPollution(progress);
            }
        });

    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.arrow_cutout);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("RatingResponse");
    }

    public static void start(Activity activity) {
        final WeakReference<Activity> context = new WeakReference<Activity>(activity);

        final Intent intent = new Intent(context.get(), RatingActivity.class);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                context.get().startActivity(intent);
            }
        }, 500);

    }

    @OnClick(R.id.btn_rate_activity_report)
    public void uploadRating() {
        RatingSyncJob.runNow(rating);
    }

}
