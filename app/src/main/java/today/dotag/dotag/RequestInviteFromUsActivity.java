package today.dotag.dotag;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.lang.ref.WeakReference;

public class RequestInviteFromUsActivity extends AppCompatActivity {

    public static void start(Activity activity) {
        WeakReference<Activity> context = new WeakReference<Activity>(activity);

        Intent intent = new Intent(context.get(), RequestInviteFromUsActivity.class);
        context.get().startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_invite_from_us);
    }
}
