package today.dotag.dotag.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andexert.library.RippleView;

import today.dotag.dotag.R;

public class DoTagButton extends RippleView {

    View rootView;
    RippleView rippleView;
    TextView textView;

    public DoTagButton(Context context) {
        super(context);
        init(context);
    }


    public DoTagButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.DoTagButton);
        String text = typedArray.getString(R.styleable.DoTagButton_button_text);
        Drawable drawable = typedArray.getDrawable(R.styleable.DoTagButton_button_background);

        setupButton(text, drawable);

    }

    private void init(Context context) {
        rootView = inflate(context, R.layout.dotag_button_layout, this);
        rippleView = rootView.findViewById(R.id.rippler_view_dotag_button);
        textView = rootView.findViewById(R.id.tv_dotag_button);
    }


    private void setupButton(String buttonText, Drawable drawable) {
        textView.setText(buttonText);
        textView.setBackground(drawable);
    }

}
