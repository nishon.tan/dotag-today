package today.dotag.dotag.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import today.dotag.dotag.R;


public class DoTagTextInput extends RelativeLayout {

    private View rootView;
    private Button btnNext;
    private TextView tvInfoText;

    public DoTagTextInput(Context context) {
        super(context);
        init(context);
    }


    public DoTagTextInput(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        rootView = inflate(context, R.layout.dotag_text_input_layout, this);
    }
}
