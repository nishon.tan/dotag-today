package today.dotag.dotag.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import today.dotag.dotag.R;

public class DoTagSlider extends RelativeLayout {

    View rootView;
    TextView sliderLabel;
    SeekBar seekBar;

    public DoTagSlider(Context context) {
        super(context);
        init(context);
    }

    public DoTagSlider(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.DoTagSlider);
        String text = typedArray.getString(R.styleable.DoTagSlider_slider_text);

        setLabel(text);
    }

    private void init(Context context) {
        rootView = inflate(context, R.layout.dotag_slider_layout, this);
        sliderLabel = rootView.findViewById(R.id.tv_dotag_slider_label);
        seekBar = rootView.findViewById(R.id.seek_bar_dotag_slider);
        seekBar.setMax(100);
    }

    public void setLabel(String label){
        sliderLabel.setText(label);
    }

    public void setOnSliderChangeListener(final valueChangeListener listener){
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                listener.onSliderChangeListener(formatProgress(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private String formatProgress(int progress) {
        return (String.valueOf(progress/10));
    }


    public interface valueChangeListener{
        void onSliderChangeListener(String progress);
    }
}
