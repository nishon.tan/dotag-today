package today.dotag.dotag.application;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

import com.evernote.android.job.JobManager;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

import timber.log.Timber;

public class Dotag extends Application {

    @SuppressLint("StaticFieldLeak")
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        Logger.addLogAdapter(new AndroidLogAdapter());
        JobManager.create(this).addJobCreator(new DataSyncJobCreator());
        context = this;
//        Timber.plant(new Timber.DebugTree() {
//            @Override
//            protected void log(int priority, String tag, String message, Throwable t) {
//                Logger.log(priority, tag, message, t);
//            }
//        });

        Timber.plant(new Timber.DebugTree());
    }

    public static Context getInstance() {
        return context;
    }
}
