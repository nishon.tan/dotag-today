package today.dotag.dotag.application;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;

import today.dotag.dotag.rating.RatingSyncJob;

public class DataSyncJobCreator implements com.evernote.android.job.JobCreator {

    @Override
    @Nullable
    public Job create(@NonNull String tag) {
        switch (tag) {
            case RatingSyncJob.TAG:
                return new RatingSyncJob();
            default:
                return null;
        }
    }
}
