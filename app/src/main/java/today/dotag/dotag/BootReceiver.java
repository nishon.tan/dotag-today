package today.dotag.dotag;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import today.dotag.dotag.alarm.AlarmReceiver;

/**
 * Created by Taman Neupane on 4/18/18.
 * for PeaceNepal Dot Com
 * contact me if any issues on taman.neupane@gmail.com
 */

// again set alarm after user has successfully booted phone
public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            Toast.makeText(context, "Again set alarm after user boots the phone", Toast.LENGTH_SHORT).show();
            AlarmReceiver alarmReceiver = new AlarmReceiver();
            alarmReceiver.setAlarm(context);
        }
    }
}

