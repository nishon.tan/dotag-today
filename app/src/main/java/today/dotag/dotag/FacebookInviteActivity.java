package today.dotag.dotag;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.model.GameRequestContent;
import com.facebook.share.widget.GameRequestDialog;

import java.lang.ref.WeakReference;

public class FacebookInviteActivity extends AppCompatActivity {

    GameRequestDialog requestDialog;
    CallbackManager callbackManager;

    public static void start(Activity activity) {
        WeakReference<Activity> context = new WeakReference<Activity>(activity);

        Intent intent = new Intent(context.get(), FacebookInviteActivity.class);
        context.get().startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook_invite);

        callbackManager = CallbackManager.Factory.create();
        requestDialog = new GameRequestDialog(this);
        requestDialog.registerCallback(callbackManager,
                new FacebookCallback<GameRequestDialog.Result>() {
                    public void onSuccess(GameRequestDialog.Result result) {
                        String id = result.getRequestId();
                    }

                    public void onCancel() {
                    }

                    public void onError(FacebookException error) {
                    }
                }
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        onClickRequestButton();
    }

    private void onClickRequestButton() {
        GameRequestContent content = new GameRequestContent.Builder()
                .setMessage("Come DoTag with me")
                .build();
        requestDialog.show(content);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

}
