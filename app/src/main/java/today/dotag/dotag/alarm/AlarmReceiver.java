package today.dotag.dotag.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.widget.Toast;

import java.util.Calendar;

import today.dotag.dotag.BootReceiver;

public class AlarmReceiver extends WakefulBroadcastReceiver {

    AlarmManager alarmMgr;
    PendingIntent alarmIntent;

    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "Alarm ringing. Now it will ring after one hour", Toast.LENGTH_SHORT).show();
    }

    //set alarm at the time of successful login in
    public void setAlarm(Context context){
        //getting alarm manager service
        alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        //registering broadcastreceiver intent to alarm manager
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        alarmIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar calendar = Calendar.getInstance();

        if (Build.VERSION.SDK_INT < 23) {
            if (Build.VERSION.SDK_INT >= 19) {
                alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                        calendar.getTimeInMillis(), AlarmManager.INTERVAL_HOUR ,alarmIntent);
            } else {
                alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP,
                        calendar.getTimeInMillis(),AlarmManager.INTERVAL_HOUR, alarmIntent);
            }
        } else {
            alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                    calendar.getTimeInMillis(),AlarmManager.INTERVAL_HOUR, alarmIntent);
        }

        //        // Enable {@code SampleBootReceiver} to automatically restart the AlertAlarm when the
//        // device is rebooted.
        // Note : BootReceiver must be in outside any package in root of project
        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }


    //cancle alarm at the time of logout
    public void cancelAlarm(Context context) {
        // If the AlertAlarm has been set, cancel it.
        if (alarmMgr != null) {
            alarmMgr.cancel(alarmIntent);
        }

        // Disable {@code SampleBootReceiver} so that it doesn't automatically restart the
        // AlertAlarm when the device is rebooted.
        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }
}
